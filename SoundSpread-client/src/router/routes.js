
const routes = [
  {
    path: '/',
    component: () => import('layouts/LoginPageLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Login/LoginIndex.vue') }
    ]
  },
  {
    path: '/Login',
    component: () => import('layouts/LoginPageLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Login/LoginIndex.vue') }
    ]
  },
  {
    path: '/Administration',
    component: () => import('layouts/SoundSpreadLayout.vue'),
    meta: { auth: true },
    children: [
      { path: '/Lists', meta: { auth: true }, component: () => import('pages/SoundSpread/ListsIndex.vue') },
      { path: '/Home', meta: { auth: true }, component: () => import('pages/SoundSpread/HomeIndex.vue') },
      { path: '/Music', meta: { auth: true }, component: () => import('pages/SoundSpread/MusicIndex.vue') }
    ]
  }

]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
