// import Vue from 'vue'

import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'

const firebaseConfig = {
  apiKey: 'AIzaSyDIh1spys1fT6xTcX6ln-zbv8GffQQ1ycM',
  authDomain: 'soundspread-b60ac.firebaseapp.com',
  projectId: 'soundspread-b60ac',
  storageBucket: 'soundspread-b60ac.appspot.com',
  messagingSenderId: '134692791891',
  appId: '1:134692791891:web:eebcf3c006e63f1f3fc8a6',
  measurementId: 'G-5LK95SQ4GW'
}

firebase.initializeApp(firebaseConfig)

export default ({ Vue }) => {
  Vue.prototype.$auth = firebase.auth()
  Vue.prototype.$db = firebase.firestore()
  Vue.prototype.$storage = firebase.storage()
}
