const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var admin = require("firebase-admin");

var serviceAccount = require("./soundspread-b60ac-firebase-adminsdk-ot9w4-95c33f0021.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});


app.get('/hello', (request, response) => {
    return response.send('Hello world');
});

app.post('/adduser', (request, response) => {
    const data = request.body;
    const user = data.user;
    console.log(user);

    return response.send('Add '+user);
});

app.put('/changeuser', (request, response) => {
    const data = request.body;
    console.log(data.user);
    return response.send('Change '+data.user);
})

app.delete('/deluser', (request, response) => {
    const data = request.body;
    console.log('Delete '+data.user);
    return response.send('Delete '+data.user);
})

app.listen(3000, () => {
    console.log("Server running on port 3000");
});
